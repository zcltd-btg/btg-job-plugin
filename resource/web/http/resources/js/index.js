var tab = $("#tab");
$(function () {
    query();
    initEvn();
})
function query() {
    loading.show();
    $.ajax({
        url: "btgjob/query.json",
        type: "post",
        data: {},
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide();
            if (isSuccess(result)) {
                tab.bootstrapTable("load", result.data)
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });
}
function initEvn() {
    var d = new Date();

    for (var i = 0; i < 60; i++) {
        var s = i < 10 ? "0" + i : i;
        var op = $("<option value='" + i + "'>" + s + "</option>")
        var sem = op.clone();
        var ses = op.clone();
        if (i < 24) {
            if (d.getHours() == i) {
                op.attr("selected", true);
            }
            $("#se_h").append(op);
        }
        if (d.getMinutes() == i) {
            sem.attr("selected", true);
        }
        $("#se_m").append(sem);
        //if (d.getSeconds() == i) {
        //    ses.attr("selected", true);
        //}
        $("#se_s").append(ses);
    }
    $("#tpye_div input[type='radio']").click(function () {
        $("div[name^='set']").hide();
        var v = $(this).attr("value");
        $("div[name='" + v + "']").show();

        if (v == 'set_d') {
            if ($("#rep_che").is(':checked')) {
                $("#rep_div").show();
            } else {
                $("#rep_div").hide();
            }
        }
        if (v == 'set_s') {
            $("#time_div").hide()
            $("#test_div").hide()
        } else {
            $("#time_div").show()
            $("#test_div").show()
        }
    })

    $("div[name=set_d] ul  a").click(function () {
        $("#btn_txt").text($(this).text())
        $("#rep_inp").attr("name", $(this).attr("name"))
    })

    $("#rep_che").click(function () {
        if ($(this).is(':checked')) {
            $("#rep_div").show();
        } else {
            $("#rep_div").hide();
        }
    })

    $('#myModal').on('shown.bs.modal', function () {
        refresh();
        var rows = tab.bootstrapTable("getSelections");
        var json = eval("(" + rows[0].param + ")")

        $("#tpye_div input[type='radio']").each(function () {
            if ($(this).attr("flag") == json.type) {
                $(this).click();
            }
        })
        if (json.type == 1) {

            $("#se_h").val(json.h)
            $("#se_m").val(json.m)
            $("#se_s").val(json.s)
            if (json.rep == "true") {
                $("#rep_che").click();

                $("#rep_inp").val(json.rep_v)

                $("div[name=set_d] ul  a").each(function () {
                    if ($(this).attr("name") == json.rep_t) {
                        $(this).click()
                    }
                })
            }


        } else if (json.type == 4) {
            $("#cron_inp").val(json.cron)
        }

    })
}
function refresh() {
    $("#rep_che").attr("checked", false);
    $("div[name=set_d] ul  a:first").click()
    $("#rep_div").hide();
    $("#rep_inp").val("")
    $("#rep_inp").attr("name", 1)
    $("#cron_inp").val("")


    var d = new Date();

    $("#se_h").val(d.getHours())
    $("#se_m").val(d.getMinutes())
    $("#se_s").val(0)
}
function stateFormat(value, row) {
    var re = "";
    switch (value) {
        case "0":
            re = "<span style='color:red'>停用</span>";
            break;
        case "1":
            re = "<span style='color:green'>启用</span>";
            break;
    }
    return re;
}
function sub() {

    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    $("#myModal").modal()
}

/**
 * 生成表达式
 */
function createcron() {
    if (!valid())
        return;
    $.ajax({
        url: "btgjob/cron.json",
        type: "post",
        data: getData(),
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide()
            if (isSuccess(result)) {
                $("#test_div span").html(result.cron)
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });
}
function getData() {
    var type = $("#tpye_div input[type='radio']:checked").attr("flag");

    var week = "";
    $("div[name='set_w'] input[type=checkbox]:checked").each(function (i, v) {
        week += "," + $(this).attr("value")
    })
    if (week.length > 0)
        week = week.substring(1);
    var month = "";
    $("div[name='set_m'] input[type=checkbox]:checked").each(function (i, v) {
        month += "," + $(this).attr("value")
    })
    if (month.length > 0)
        month = month.substring(1);
    var data = {
        type: type,
        h: $("#se_h").val(),
        m: $("#se_m").val(),
        s: $("#se_s").val(),
        rep: $("#rep_che").is(":checked"),
        rep_v: $("#rep_inp").val(),
        rep_t: $("#rep_inp").attr("name"),
        week: week,
        month: month
    }
    return data;
}
function valid() {
    var msg = "";
    var type = $("#tpye_div input[type='radio']:checked").attr("flag");
    if (type == 4) {
        if ($("#cron_inp").val().trim() == "") {
            $("#cron_inp").val("")
            msg = "请输入cron表达式"
            layer.tips(msg, '#cron_inp', {tips: [3, 'orange']});
            return false;
        }
    } else if (type == 1) {
        if ($("#rep_che").is(":checked")) {
            var reg_h = "^(?:1?\\d|2[0-3])$"
            var reg_m = "^[1-5]?\\d$";
            var rep_v = $("#rep_inp").val()
            var rep_t = $("#rep_inp").attr("name")
            if (rep_t == 1) {
                if (!rep_v.match(reg_h)) {
                    msg = "只能输入0~23的数字"
                    layer.tips(msg, '#rep_inp', {tips: [3, 'orange']});
                    return false;
                }
            } else {
                if (!rep_v.match(reg_m)) {
                    msg = "只能输入0~59的数字"
                    layer.tips(msg, '#rep_inp', {tips: [3, 'orange']});
                    return false;
                }
            }
        }
    }

    return true;

}

function commit() {

    if (!valid())
        return;
    var rows = tab.bootstrapTable("getSelections");
    var id = rows[0]['id']
    var data = getData();
    data.id = id;
    data.cron = data.type == 4 ? $("#cron_inp").val() : "";
    $.ajax({
        url: "btgjob/commit.json",
        type: "post",
        data: data,
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide()
            if (isSuccess(result)) {
                $('#myModal').modal('hide')
                query();
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });
}

function changeState(con) {
    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    confirmMsg("是否更改选中任务的状态？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgjob/changeState.json",
                type: "post",
                data: {
                    con: con,
                    id: rows[0]['id']
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        query();
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });
}