$(document).ajaxComplete(function (event, xhr, settings) {
    var resText = xhr.responseText;

    var start = resText.indexOf('>');
    if (start != -1) {
        var end = resText.indexOf('<', start + 1);
        if (end != -1) {
            resText = resText.substring(start + 1, end);
        }
    }
    var v = eval("(" + resText + ")");
    if (v.code == -1 && v.msg == 'SessionTimeOut') {
        alertMsg('用户登录会话已过期，请重新登录！', {
            callback: function () {
                top.window.location.href = v.url;
            }
        });
    } else {
        if (v.key) {
            setCookie(v.key, v[v.key])
        }
    }
});
var nullfunction = function () {

};
function getCookie(name) {
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if (arr != null) {
        return unescape(arr[2]);
    } else {
        return null;
    }
}
function setCookie(name, value) {
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

var alertMsg = function (msg, options) {
    options = $.type(options) == "undefined" ? {} : options;

    var title = $.type(options["title"]) == "undefined" ? '系统提示：' : options["title"];
    var isTop = $.type(options["isTop"]) == "undefined" ? true : options["isTop"];
    var icon = $.type(options["icon"]) == "undefined" ? 0 : options["icon"];
    var area = $.type(options["area"]) == "undefined" ? 'auto' : options["area"];

    var targetWindow = isTop == true ? top : window;
    targetWindow.layer.alert(msg, {
        title: title,
        icon: icon,
        anim: 9,
        skin: 'layer-ext-moon',
        closeBtn: 0,//不显示关闭按钮
        area: area
    }, function (index) {
        targetWindow.layer.close(index);

        var callback = options["callback"];
        if ($.type(callback) == "function") {
            callback.call(this);
        }
    })
}

var confirmMsg = function (msg, options) {
    options = $.type(options) == "undefined" ? {} : options;

    var isTop = $.type(options["isTop"]) == "undefined" ? true : options["isTop"];
    var yesCallback = $.type(options["yes"]) == "function" ? options["yes"] : {};
    var noCallback = $.type(options["no"]) == "function" ? options["no"] : {};

    var targetWindow = isTop == true ? top : window;
    targetWindow.layer.confirm(msg, {
        icon: 3,
        anim: 9,
        btn: ['是', '否']
    }, function (index) {
        if ($.type(yesCallback) == "function") {
            yesCallback.call(this);
        }
        targetWindow.layer.close(index);
    }, function () {
        if ($.type(noCallback) == "function") {
            noCallback.call(this);
        }
    });
}

var promptInput = function (options) {
    options = $.type(options) == "undefined" ? {} : options;

    var isTop = $.type(options["isTop"]) == "undefined" ? true : options["isTop"];
    var formType = $.type(options["formType"]) == "undefined" ? 0 : options["formType"];
    var title = $.type(options["title"]) == "undefined" ? '请输入值' : options["title"];
    var value = $.type(options["value"]) == "undefined" ? '' : options["value"];

    var targetWindow = isTop == true ? top : window;
    targetWindow.layer.prompt({
        formType: formType,
        anim: 9,
        title: title,
        value: value
    }, function (value, index, elem) {
        var isCheck = true;

        var checkFn = options["check"];
        if ($.type(checkFn) == "function") {
            var ic = checkFn.call(this, value, index, $(elem));
            if ($.type(ic) == 'boolean') {
                isCheck = ic;
            }
        }

        if (isCheck == true) {
            targetWindow.layer.close(index);
            var callback = options["callback"];
            if ($.type(callback) == "function") {
                callback.call(this, value, index, $(elem));
            }
        }
    });
}

var alertMsgError = function (msg, options) {
    alertMsg(msg, $.extend(true, {icon: 2}, options || {}));
}

var loading = {};
loading.show = function (options) {
    options = $.type(options) == "undefined" ? {} : options;
    var isTop = $.type(options["isTop"]) == "undefined" ? false : options["isTop"];
    var targetWindow = isTop == true ? top : window;
    targetWindow.layer.load(0);
}
loading.hide = function (options) {
    options = $.type(options) == "undefined" ? {} : options;
    var isTop = $.type(options["isTop"]) == "undefined" ? false : options["isTop"];
    var targetWindow = isTop == true ? top : window;
    targetWindow.layer.closeAll('loading');
}
loading.hide();

var changeUrl = function (url, options) {
    options = $.type(options) == "undefined" ? {} : options;

    var isTop = $.type(options["isTop"]) == "undefined" ? false : options["isTop"];
    var callback = $.type(options["callback"]) == "function" ? options["callback"] : {};
    var isNew = $.type(options["isNew"]) == "boolean" ? options["isNew"] : false;

    var targetWindow = isTop == true ? top : window;

    if (isNew == true) {
        targetWindow.open(url);
    } else {
        targetWindow.location.href = url;
    }

    if ($.type(callback) == "function") {
        callback.call(this);
    }
}

var isSuccess = function (result) {
    if ($.type(result) != 'object') {
        alertMsg("返回数据结构错误，解析失败！");
        return false;
    }
    if ($.type(result['code']) != 'string') {
        alertMsg("返回数据结构错误，解析[code]失败！");
        return false;
    }

    if (result['code'] != '0000') return false;

    return true;
}

var onajaxerror = function (jqXHR, textStatus, errorThrown) {
    loading.hide();
    alertMsgError(jqXHR.responseText, {
        callback: function () {
        }
    });
}

var onAjaxRequestFailure = function (result, callback) {
    callback = $.type(callback) == "function" ? callback : function () {
    };
    if ($.type(result) != 'object') {
        alertMsg("返回数据结构错误，解析失败！", {callback: callback});
        return;
    }
    if ($.type(result['code']) != 'string') {
        alertMsg("返回数据结构错误，解析[code]失败！", {callback: callback});
        return;
    }
    if ($.type(result['msg']) != 'string') {
        alertMsg("返回数据结构错误，解析[msg]失败！", {callback: callback});
        return;
    }
    //alertMsg('[' + result['code'] + ']' + result['msg'], {callback: callback});
    alertMsg(result['msg'], {callback: callback});
}

//禁用objs输入自动补全的历史记录
var disableAutocomplete = function (objs) {
    $(objs).attr('autocomplete', 'off')
    $(objs).prop('autocomplete', 'off')
}

//判断是否相等
var equals = function (arg0, arg1) {
    if ($.type(arg0) == 'string' && $.type(arg1) == 'string') {
        return arg0.trim() == arg1.trim();
    }
    return arg0 == arg1;
}

/*
 常用正则表达式
 */
var REGEX = {};
REGEX.INT = /^\d+$/;
REGEX.DOUBLE = /^\d+(\.\d{1,2})?$/;
REGEX.DOUBLE_3 = /^\d+(\.\d{1,3})?$/;
REGEX.DOUBLE_4 = /^\d+(\.\d{1,4})?$/;

/*
 获取时间
 */
var SYSDATE = function (date, asNum) {
    date = $.type(date) == 'date' ? date : $.type(date) == 'string' ? new Date(date) : new Date();
    var result = {};
    result.NOW = date;
    result.MILLIS = result.NOW.getTime();
    result.DATE = result.NOW.toLocaleDateString();
    result.TIME = result.NOW.toLocaleTimeString();
    result.DATETIME = result.NOW.toLocaleString();
    result.WEEK = result.NOW.getDay();

    result.YYYY = result.NOW.getFullYear();
    result.MM = result.NOW.getMonth() + 1;
    result.DD = result.NOW.getDate();

    result.HH = result.NOW.getHours();
    result.MI = result.NOW.getMinutes();
    result.SS = result.NOW.getSeconds();
    result.SSS = result.NOW.getMilliseconds();

    if ($.type(asNum) == 'undefined' || asNum == false) {
        result.MM = parseInt(result.MM) < 10 ? '0' + result.MM : result.MM;
        result.DD = parseInt(result.DD) < 10 ? '0' + result.DD : result.DD;
        result.HH = parseInt(result.HH) < 10 ? '0' + result.HH : result.HH;
        result.MI = parseInt(result.MI) < 10 ? '0' + result.MI : result.MI;
        result.SS = parseInt(result.SS) < 10 ? '0' + result.SS : result.SS;
    }

    result.addDays = function (n) {
        return new Date(this.MILLIS + n * 24 * 60 * 60 * 1000);
    };
    return result;
};

var btg = $.extend({}, {
    open: function (options) {
        options = $.type(options) == "undefined" ? {} : options;
        var isTop = $.type(options["isTop"]) == "undefined" ? true : options["isTop"];

        var title = $.type(options["title"]) == "undefined" ? '新窗口' : options["title"];
        var url = $.type(options["url"]) == "undefined" ? '' : options["url"];

        var max = $.type(options["max"]) == "undefined" ? false : options["max"];
        var width = max == true ? $(top.window).width() : ($.type(options["width"]) == "undefined" ? 800 : options["width"]);
        var height = max == true ? $(top.window).height() : ($.type(options["height"]) == "undefined" ? 500 : options["height"]);

        var btn = $.type(options["btn"]) == "undefined" ? ['确定', '关闭'] : options["btn"];
        var btn2Fn = $.type(options["btn2Fn"]) == "undefined" ? nullfunction : options["btn2Fn"];
        var btn3Fn = $.type(options["btn3Fn"]) == "undefined" ? nullfunction : options["btn3Fn"];

        var data = $.type(options["data"]) == "undefined" ? {} : options["data"];
        var setData = $.type(options["setData"]) == "undefined" ? "setData" : options["setData"];
        var doSubmit = $.type(options["doSubmit"]) == "undefined" ? "doSubmit" : options["doSubmit"];
        var closeWindow = $.type(options["closeWindow"]) == "undefined" ? "closeWindow" : options["closeWindow"];

        var onload = $.type(options["onload"]) == "undefined" ? nullfunction : options["onload"];
        var ondestroy = $.type(options["ondestroy"]) == "undefined" ? nullfunction : options["ondestroy"];

        var targetWindow = isTop == true ? top : window;
        return targetWindow.layer.open($.extend({
            type: 2,
            title: title,
            content: url,
            area: [width + "px", height + "px"],
            btn: btn,
            anim: 9,
            maxmin: true,
            closeBtn: 1,
            resize: false,
            success: function (layero, index) {
                var iframeWin = layero.find('iframe')[0].contentWindow;
                data[closeWindow] = function (data) {
                    targetWindow.layer.close(index);
                    ondestroy(data);
                }
                if ($.type(iframeWin[setData]) == 'function') {
                    iframeWin[setData].call(this, data);
                }
                onload.call(this, iframeWin);
            },
            yes: function (index, layero) {
                var iframeWin = layero.find('iframe')[0].contentWindow;
                if ($.type(iframeWin[doSubmit]) == 'function') {
                    iframeWin[doSubmit].call(this);
                }
            },
            cancel: function (index) {
                btn2Fn.call(this, index);
            },
            btn3: function (index) {
                btn3Fn.call(this, index);
            }
        }, options));
    },
    selectIcon: function (options) {
        options = $.type(options) == "undefined" ? {} : options;

        var callback = $.type(options["callback"]) == "undefined" ? nullfunction : options["callback"];

        btg.open($.extend({
            title: "选择图标",
            url: basePATH + "/page/common/icon_select.html",
            btn: false,
            ondestroy: function (data) {
                callback.call(this, data);
            }
        }, options));
    }
});
var getNowDate = function (format) {
    var f = "yyyy-MM-dd"
    if (format)
        f = format
    var st = new Date();
    var n = formatDate(st, f);
    return n;

}
var getMonthFirstDate = function (format) {
    var f = "yyyy-MM-dd"
    if (format)
        f = format
    var st = new Date();
    st.setDate(1);
    var n = formatDate(st, f);
    return n;

}
//格式化日期,
function formatDate(date, format) {
    var paddNum = function (num) {
        num += "";
        return num.replace(/^(\d)$/, "0$1");
    }
    //指定格式字符
    var cfg = {
        yyyy: date.getFullYear() //年 : 4位
        , yy: date.getFullYear().toString().substring(2)//年 : 2位
        , M: date.getMonth() + 1  //月 : 如果1位的时候不补0
        , MM: paddNum(date.getMonth() + 1) //月 : 如果1位的时候补0
        , d: date.getDate()   //日 : 如果1位的时候不补0
        , dd: paddNum(date.getDate())//日 : 如果1位的时候补0
        , hh: date.getHours()  //时
        , mm: date.getMinutes() //分
        , ss: date.getSeconds() //秒
    }
    format || (format = "yyyy-MM-dd hh:mm:ss");
    return format.replace(/([a-z])(\1)*/ig, function (m) {
        return cfg[m];
    });
}
function stopDefault(e) {
    if (e && e.preventDefault) {//如果是FF下执行这个
        e.preventDefault();
    } else {
        window.event.returnValue = false;//如果是IE下执行这个
    }

    return false;
}