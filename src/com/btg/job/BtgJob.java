package com.btg.job;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.btg.job.core.CoreJob;
import com.btg.job.core.Model;
import com.btg.job.core.QuartzManager;
import com.btg.job.util._Utils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.quartz.SchedulerException;

import java.io.*;
import java.text.ParseException;
import java.util.List;
import java.util.Map;


/**
 * Created by Administrator on 2017/2/22.
 */
public class BtgJob extends CoreJob {
    private static BtgJob job = null;

    private BtgJob() {
        super();
    }


    public static BtgJob instance() {
        if (job == null) {
            synchronized (BtgJob.class) {
                if (job == null)
                    job = new BtgJob();
            }
        }
        return job;
    }

    public BtgJob setXmlPath(String xmlPath) {
        this.xmlPath = handlePath(xmlPath);
        return instance();
    }

    public String getXmlPath() {
        return this.xmlPath;
    }

    public int getSessionTimeOut() {
        return this.sessionTimeOut;
    }

    public File getXmlFile() {
        return this.configFile;
    }

    public JSONArray getXmlJson() {
        return this.xmlJson;
    }

    public Map<String, JSONObject> getXmlMap() {
        return this.xmlMap;
    }

    public Map<String, Model> getJobMap() {
        return this.jobMap;
    }

    public BtgJob setSessionTimeOut(int sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
        return instance();
    }

    public boolean getIsRefreshSession() {
        return this.isRefreshSession;
    }

    public BtgJob setIsRefreshSession(boolean isRefreshSession) {
        this.isRefreshSession = isRefreshSession;
        return instance();
    }

    public BtgJob addJob(Model model) {
        if (model != null) {
            if (model.getJobName() == null || "".equals(model.getJobName())) {
                throw new RuntimeException("任务名称不能为null");
            }
            String k = _Utils.calcMD5(model.getJobName());
            if (jobMap.keySet().contains(k))
                throw new RuntimeException("任务名称：" + model.getJobName() + "重复");
            jobMap.put(k, model);
        }
        return instance();
    }

    /**
     * 根据任务列表生成xml配置文件
     */
    private void handleJobXML() {

        if (this.xmlPath == null || "".equals(this.xmlPath)) {
            this.xmlPath = handlePath(this.getClass().getResource("/").getPath());
        }

        File dir = new File(this.xmlPath);
        if (!dir.isDirectory()) {
            throw new RuntimeException("任务配置文件夹地址：" + this.xmlPath + "。不存在");
        }
        String path = this.xmlPath + "/job-config.xml";
        File xml = new File(path);
        try {
            if (!xml.exists()) {//创建任务配置文件
                xml = createXml(path);
            }
            this.configFile = xml;

            SAXReader reader = new SAXReader();
            Document doc = reader.read(xml);
            Element root = doc.getRootElement();

            List<Node> jobNodeList = root.selectNodes("job[@id]");

            for (String k : jobMap.keySet()) {

                Model model = jobMap.get(k);

                Node jobNode = root.selectSingleNode("job[@id='" + k + "']");
                Element el, nameEl, descEl, stateEl;
                if (jobNode == null) {
                    el = root.addElement("job");
                    el.addAttribute("id", k);
                    jobNode = el;
                } else {
                    el = (Element) jobNode;
                    if (jobNodeList.contains(jobNode)) {
                        jobNodeList.remove(jobNode);
                    }
                }
                Node node = jobNode.selectSingleNode(jobName);
                if (node == null) {
                    nameEl = el.addElement(jobName);
                } else {
                    nameEl = (Element) node;
                }
                nameEl.setText(model.getJobName());
                node = jobNode.selectSingleNode(jobDesc);
                if (node == null) {
                    descEl = el.addElement(jobDesc);
                } else {
                    descEl = (Element) node;
                }
                descEl.setText(model.getJobDesc() == null ? "" : model.getJobDesc());

                node = jobNode.selectSingleNode(jobState);
                if (node == null) {
                    stateEl = el.addElement(jobState);
                    stateEl.setText("1");
                } else {
                    stateEl = (Element) node;
                }
            }
            for (Node n : jobNodeList) {
                root.remove(n);
            }
            modifyXml(path, doc);//保存修改后的xml配置文件
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 更改任务
     *
     * @param id 任务id
     */
    public void modifyJob(String id) {
        try {
            JSONObject object = BtgJob.instance().getXmlMap().get(id);
            if (object != null) {
                QuartzManager.removeJob(object.getString(jobName));

                if ("1".equals(object.getString(jobState))) {//启动任务
                    Model m = BtgJob.instance().getJobMap().get(id);
                    QuartzManager.addJob(m.getJobName(), m.getJobClass(), object.getString(jobCron));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void run() {
        handleJobXML();//处理xml文件

        for (int i = 0; i < this.xmlJson.size(); i++) {
            JSONObject obj = this.xmlJson.getJSONObject(i);
            if (obj.getString(jobCron) != null && !"".equals(obj.getString(jobCron)) && "1".equals(obj.getString(jobState)))
                try {
                    QuartzManager.addJob(obj.getString(jobName), jobMap.get(obj.getString(id)).getJobClass(), obj.getString(jobCron));
                } catch (SchedulerException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
        }
    }

    public static void main(String[] args) {
    }


}
