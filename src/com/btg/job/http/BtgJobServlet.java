package com.btg.job.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.btg.job.BtgJob;
import com.btg.job.core.QuartzManager;
import com.btg.job.util._Utils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.quartz.*;
import org.quartz.Trigger.TriggerState;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

/**
 * Created by Administrator on 2017/2/22.
 */
public class BtgJobServlet extends ResourceServlet {

    public BtgJobServlet() {
        super("web/http/resources");
    }


    /**
     * 登录
     *
     * @param params
     * @param req
     * @param resp
     * @return
     */
    public JSONObject login(JSONObject params, HttpServletRequest req, HttpServletResponse resp) {

        JSONObject re = new JSONObject();

        Properties p = new Properties();
        InputStream in = null;
        try {
            in = this.getClass().getResourceAsStream("/com/btg/job/config/login.properties");
            p.load(in);

            String loginId = params.getString("login_id");
            String loginPwd = params.getString("login_pwd");

            if (!p.getProperty("login_name").equals(loginId)) {
                re.put("msg", "用户名错误");
                return error(re);
            }
            if (!p.getProperty("login_pwd").equals(loginPwd)) {
                re.put("msg", "密码错误");
                return error(re);
            }
            re.put(keyName, _Utils.getKey(System.currentTimeMillis()));
            re.put("key", keyName);
            return succ(re);
        } catch (IOException e) {
            e.printStackTrace();
            re.put("msg", "读取用户配置文件失败");
            return error(re);
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public JSONObject query(JSONObject params, HttpServletRequest req, HttpServletResponse resp) {
        JSONObject reJson = new JSONObject();
        JSONArray arr = BtgJob.instance().getXmlJson();

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        try {
            Scheduler sched = QuartzManager.sf.getScheduler();

            TriggerKey key;
            Trigger trigger;
            TriggerState ts;
            for (int i = 0; i < arr.size(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                String jobName = obj.getString(BtgJob.jobName);
                key = new TriggerKey(_Utils.calcMD5(jobName), QuartzManager.TRIGGER_GROUP_NAME);
                trigger = sched.getTrigger(key);
                if (trigger != null) {
                    obj.put("last_time", trigger.getPreviousFireTime() != null ? sf.format(trigger.getPreviousFireTime()) : "");
                    obj.put("next_time", trigger.getNextFireTime() != null ? sf.format(trigger.getNextFireTime()) : "");
                    ts = sched.getTriggerState(key);
                    String st = "错误";
                    switch (ts) {
                        case NONE:
                            st = "任务已结束";
                            break;
                        case NORMAL:
                            st = "正常状态";
                            break;
                        case PAUSED:
                            st = "暂停状态";
                            break;
                        case COMPLETE:
                            st = "触发器完成，但是任务可能还正在执行中";
                            break;
                        case BLOCKED:
                            st = "线程阻塞状态";
                            break;
                        case ERROR:
                            st = "出现错误";
                            break;
                    }
                    obj.put("trigger", st);
                }
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        reJson.put("data", arr);
        return succ(reJson);
    }

    public JSONObject cron(JSONObject params, HttpServletRequest req, HttpServletResponse resp) {
        JSONObject reJson = new JSONObject();
        int type = params.getInteger("type");
        int h = params.getInteger("h");
        int m = params.getInteger("m");
        int s = params.getInteger("s");

        String em = " ";

        String cron = "";
        try {
            switch (type) {
                case 1:
                    Calendar c = Calendar.getInstance();

                    if (params.getBoolean("rep")) {//重复
                        int rept = params.getInteger("rep_t");
                        int repv = params.getInteger("rep_v");
                        switch (rept) {
                            case 1:
                                cron = s + em + m + em + h + "/" + repv + em + "* * ?";
                                break;
                            case 2:
                                cron = s + em + m + "/" + repv + em + "* * * ?";
                                break;
                            case 3:
                                cron = s + "/" + repv + em + "* * * * ?";
                                break;
                        }
                    } else {
                        cron = s + em + m + em + h + em + "* * ?";
                    }
                    break;
                case 2:
                    break;
                case 3:
            }
            reJson.put("cron", cron);
            return succ(reJson);
        } catch (Exception e) {
            e.printStackTrace();
            reJson.put("msg", "生成cron表达式失败");
            return error(reJson);
        }
    }

    public JSONObject commit(JSONObject params, HttpServletRequest req, HttpServletResponse resp) {
        JSONObject reJson = new JSONObject();
        try {

            int type = params.getInteger("type");
            String cron;
            if (type != 4) {
                JSONObject obj = cron(params, req, resp);
                cron = obj.getString(BtgJob.jobCron);
            } else {
                cron = params.getString(BtgJob.jobCron);
            }
            String id = params.getString(BtgJob.id);

            File f = BtgJob.instance().getXmlFile();

            SAXReader reader = new SAXReader();
            Document doc = reader.read(f);
            Element root = doc.getRootElement();

            Node node = root.selectSingleNode("job[@id='" + id + "']");

            Element cronEl, jobEl, paramEl;
            jobEl = (Element) node;
            Node cronNode = node.selectSingleNode(BtgJob.jobCron);
            if (cronNode == null) {
                cronEl = jobEl.addElement(BtgJob.jobCron);
            } else {
                cronEl = (Element) cronNode;
            }
            cronEl.setText(cron);

            Node paramNode = node.selectSingleNode(BtgJob.jobParam);
            if (cronNode == null) {
                paramEl = jobEl.addElement(BtgJob.jobParam);
            } else {
                paramEl = (Element) paramNode;
            }
            paramEl.setText(params.toString());

            BtgJob.instance().modifyXml(f.getPath(), doc);


            BtgJob.instance().modifyJob(id);
            return succ(reJson);
        } catch (Exception e) {
            e.printStackTrace();
            reJson.put("msg", "保存失败");
            return error(reJson);
        }
    }

    public JSONObject changeState(JSONObject params, HttpServletRequest req, HttpServletResponse resp) {
        JSONObject reJson = new JSONObject();
        try {

            String con = params.getString("con");
            String id = params.getString("id");

            File f = BtgJob.instance().getXmlFile();

            SAXReader reader = new SAXReader();
            Document doc = reader.read(f);
            Element root = doc.getRootElement();

            Node node = root.selectSingleNode("job[@id='" + id + "']");


            Element state = (Element) node.selectSingleNode(BtgJob.jobState);
            state.setText(con);
            BtgJob.instance().modifyXml(f.getPath(), doc);

            BtgJob.instance().modifyJob(id);//修改任务
            return succ(reJson);
        } catch (Exception e) {
            e.printStackTrace();
            reJson.put("msg", "保存失败");
            return error(reJson);
        }
    }


    private JSONObject succ(JSONObject reJeson) {
        reJeson.put("code", "0000");
        if (!reJeson.containsKey("msg"))
            reJeson.put("msg", "请求成功！");
        return reJeson;
    }

    private JSONObject error(JSONObject reJeson) {
        reJeson.put("code", "9999");
        if (!reJeson.containsKey("msg"))
            reJeson.put("msg", "请求失败！");
        return reJeson;
    }
}
