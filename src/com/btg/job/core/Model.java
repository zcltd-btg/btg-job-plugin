package com.btg.job.core;


/**
 * Created by Administrator on 2017/2/22.
 */
public class Model {

    private String jobName;
    private String jobDesc;

    private  Class<? extends QuartzJob> jobClass;

    private Model() {
    }

    public Model(String jobName,  Class<? extends QuartzJob> jobClass) {
        this.jobName = jobName != null ? jobName.trim() : jobName;
        this.jobClass = jobClass;
    }

    public Model(String jobName,  Class<? extends QuartzJob> jobClass, String jobDesc) {
        this.jobName = jobName != null ? jobName.trim() : jobName;
        this.jobClass = jobClass;
        this.jobDesc = jobDesc != null ? jobDesc.trim() : jobDesc;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobDesc() {
        return jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    public Class<? extends QuartzJob> getJobClass() {
        return jobClass;
    }

    public void setJobClass(Class<? extends QuartzJob> jobClass) {
        this.jobClass = jobClass;
    }
}
