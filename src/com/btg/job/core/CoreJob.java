package com.btg.job.core;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.util.*;

/**
 * Created by Administrator on 2017/2/22.
 */
public class CoreJob {
    protected CoreJob() {
    }

    protected int sessionTimeOut = 30;//sesssion失效时间
    protected boolean isRefreshSession = true;//是否每次操作都刷新session
    protected String xmlPath;

    protected File configFile;

    protected JSONArray xmlJson = new JSONArray();

    protected Map<String, JSONObject> xmlMap = new HashMap<String, JSONObject>();

    protected Map<String, Model> jobMap = new TreeMap<String, Model>();

    public static String id = "id", jobName = "job-name", jobDesc = "job-desc", jobState = "state", jobCron = "cron", jobParam = "param";

    protected String handlePath(String path) {
        if (path == null)
            return "";
        String p = path.substring(path.length() - 1);
        if ("\\".equals(p) || "/".equals(p)) {
            path = path.substring(0, path.length() - 1);
            path = handlePath(path);
        }
        return path;
    }


    protected File createXml(String path) throws IOException, DocumentException {
        Document doc = DocumentHelper.createDocument();
        doc.addElement("config");
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(path), "utf-8");
        doc.write(out);
        out.close();
        getXmlData(path);
        return new File(path);
    }

    public File modifyXml(String path, Document doc) throws IOException, DocumentException {
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(path), "utf-8");
        doc.write(out);
        out.close();
        getXmlData(path);
        return new File(path);
    }

    public JSONArray getXmlData(String path) throws DocumentException {
        JSONArray arr = new JSONArray();
        SAXReader reader = new SAXReader();
        Document doc = reader.read(new File(path));
        Element root = doc.getRootElement();
        List<Node> list = root.selectNodes("job");

        for (Node node : list) {
            JSONObject json = new JSONObject();
            Element el = (Element) node;
            json.put("id", el.attributeValue("id"));
            if (node.selectSingleNode(jobName) != null) {
                el = (Element) node.selectSingleNode(jobName);
                json.put(jobName, el.getText());
            } else {
                json.put(jobName, "");
            }
            if (node.selectSingleNode(jobDesc) != null) {
                el = (Element) node.selectSingleNode(jobDesc);
                json.put(jobDesc, el.getText());
            } else {
                json.put(jobDesc, "");
            }
            if (node.selectSingleNode(jobState) != null) {
                el = (Element) node.selectSingleNode(jobState);
                json.put(jobState, el.getText());
            } else {
                json.put(jobState, "");
            }
            if (node.selectSingleNode(jobCron) != null) {
                el = (Element) node.selectSingleNode(jobCron);
                json.put(jobCron, el.getText());
            } else {
                json.put(jobCron, "");
            }
            if (node.selectSingleNode(jobParam) != null) {
                el = (Element) node.selectSingleNode(jobParam);
                json.put(jobParam, el.getText());
            } else {
                json.put(jobParam, "");
            }
            arr.add(json);

            xmlMap.put(json.getString("id"), json);
        }
        xmlJson = arr;
        return arr;
    }

}
