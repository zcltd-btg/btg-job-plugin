package com.btg.job.core;

import java.text.ParseException;
import java.util.Map;

import com.btg.job.util._Utils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzManager {
    public static SchedulerFactory sf = new StdSchedulerFactory();
    public static String JOB_GROUP_NAME = "DEFAULT_JOB_GROUP";
    public static String TRIGGER_GROUP_NAME = "DEFAULT_TRIGGER_GROUP";


    /** */
    /**
     * 添加一个定时任务，使用默认的任务组名，触发器名，触发器组名
     *
     * @param jobName  任务名
     * @param jobClass 任务
     * @param time     时间设置，参考quartz说明文档
     * @throws SchedulerException
     * @throws ParseException
     */
    public static void addJob(String jobName, Class<? extends QuartzJob> jobClass, String time)
            throws SchedulerException, ParseException {
        addJob(jobName, JOB_GROUP_NAME, _Utils.calcMD5(jobName), TRIGGER_GROUP_NAME, jobClass, time);
    }

    private static void addJob(String jobName, String jobGroupName,
                               String triggerName, String triggerGroupName,
                               Class<? extends QuartzJob> jobClass, String cron) {
        try {
            Scheduler sched = sf.getScheduler();

            //用于描叙Job实现类及其他的一些静态信息，构建一个作业实例
            JobDetail jobDetail = JobBuilder.newJob(jobClass)
                    .withIdentity(jobName, jobGroupName)
                    .build();

            //描叙触发Job执行的时间触发规则,Trigger实例化一个触发器
            Trigger trigger = TriggerBuilder.newTrigger()//创建一个新的TriggerBuilder来规范一个触发器
                    .withIdentity(triggerName, triggerGroupName)//给触发器一个名字和组名
                            //.startNow()//立即执行
                            //.startAt(runTime)//设置触发开始的时间
                    .withSchedule
                            (
                                    CronScheduleBuilder.cronSchedule(cron)
                            )
                    .build();//产生触发器
            //向Scheduler添加一个job和trigger
            sched.scheduleJob(jobDetail, trigger);
//            // 启动
            if (!sched.isShutdown()) {
                sched.start();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    public static void modifyJobTime(String triggerName, String triggerGroupName,
                                     String cron)
            throws SchedulerException, ParseException {
        try {
            Scheduler sched = sf.getScheduler();
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
            CronTrigger trigger = (CronTrigger) sched.getTrigger(triggerKey);
            if (trigger == null) {
                return;
            }
            String oldTime = trigger.getCronExpression();
            if (!oldTime.equalsIgnoreCase(cron)) {
                /** 方式一 ：调用 rescheduleJob 开始 */
                // 触发器
                TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
                // 触发器名,触发器组
                triggerBuilder.withIdentity(triggerName, triggerGroupName);
                triggerBuilder.startNow();
                // 触发器时间设定
                triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));
                // 创建Trigger对象
                trigger = (CronTrigger) triggerBuilder.build();
                // 方式一 ：修改一个任务的触发时间
                sched.rescheduleJob(triggerKey, trigger);
                /** 方式一 ：调用 rescheduleJob 结束 */

                /** 方式二：先删除，然后在创建一个新的Job  */
                //JobDetail jobDetail = sched.getJobDetail(JobKey.jobKey(jobName, jobGroupName));
                //Class<? extends Job> jobClass = jobDetail.getJobClass();
                //removeJob(jobName, jobGroupName, triggerName, triggerGroupName);
                //addJob(jobName, jobGroupName, triggerName, triggerGroupName, jobClass, cron);
                /** 方式二 ：先删除，然后在创建一个新的Job */
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /** */
    /**
     * 移除一个任务(使用默认的任务组名，触发器名，触发器组名)
     *
     * @param jobName
     * @throws SchedulerException
     */
    public static void removeJob(String jobName)
            throws SchedulerException {
        removeJob(jobName, JOB_GROUP_NAME, _Utils.calcMD5(jobName), TRIGGER_GROUP_NAME);
    }


    private static void removeJob(String jobName, String jobGroupName,
                                  String triggerName, String triggerGroupName)
            throws SchedulerException {
        try {
            Scheduler sched = sf.getScheduler();
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
            sched.pauseTrigger(triggerKey);// 停止触发器
            sched.unscheduleJob(triggerKey);// 移除触发器
            sched.deleteJob(JobKey.jobKey(jobName, jobGroupName));// 删除任务
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 启动所有定时任务
     */
    public static void startJobs() {
        try {
            Scheduler sched = sf.getScheduler();
            sched.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭所有定时任务
     */
    public static void shutdownJobs() {
        try {
            Scheduler sched = sf.getScheduler();
            if (!sched.isShutdown()) {
                sched.shutdown();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String,Trigger> getAllTrigger(){
        try {
            Scheduler sched = sf.getScheduler();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

}