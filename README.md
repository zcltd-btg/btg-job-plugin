# btg-job-plugin

#### 项目介绍
> 提供对项目中的quartz任务进行可视化管理

#### 升级记录
```
v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.3
1、btg-parent升级到v2.0.1；

v3.0.2
1、统一依赖管理

v3.0.1
1、转为maven项目；
2、统一迁移至公司名下；

V1.0.2
1)在启动项目时根据之前的任务启停状态进行任务的启动

V1.0.1
初始化
```

#### 使用说明
```
1)添加任务
    Model m = new Model("测试任务", TestJob.class, "备注");//TestJob需要实现QuartzJob接口
    BtgJob.instance().addJob(m);

2)启动配置信息
    BtgJob.instance().run();

访问任务插件配置页
http://xxx.xxx.xxx.xxx/xxx/btgjob

默认登录账号  admin
默认登录密码  admin123!@#
```